function txt = myupdatefcn( ~,event_obj)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

global result;
global result_ab;

pos = get(event_obj,'Position');

res = result(pos(2),pos(1));
res2 = result_ab(pos(2),pos(1));

txt = {['X: ',num2str(pos(1))],['Y: ',num2str(pos(2))],['Relative stiffness: ',num2str(res)],['Absolute stiffness: ',num2str(res2)]};

end

