function [ red,green,blue ] = separate_color_bar( color_bar )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

a=1;
b=1;
c=1;

for i = 1:size(color_bar,1)
    
    for j = 1:size(color_bar,2)
        
        [~,main_color] = max(color_bar(i,j,:));
        
        if main_color == 1
            
            red(a,1:3) = color_bar(i,j,:);
            red(a,4) = i;
            red(a,5) = j;
            a = a+1;
            
        elseif main_color == 2
            
            blue(b,1:3) = color_bar(i,j,:);
            blue(b,4) = i;
            blue(b,5) = j;
            b = b+1;
            
        elseif main_color == 3
            
            green(c,1:3) = color_bar(i,j,:);
            green(c,4) = i;
            green(c,5) = j;
            c = c+1;
            
        end
        
    end
    
end

