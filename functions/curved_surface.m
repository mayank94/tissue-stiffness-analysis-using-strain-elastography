function [splitter] = curved_surface(colored)

bw_im = mean(colored,3);
midpt_signal = bw_im(:,size(bw_im,2)/2);
for i = 1:size(midpt_signal,1)
    if(midpt_signal(i)==0)
        splitter=i;
        break;
    end
end
%splitter = ones(1, size(colored,2))*pos;
