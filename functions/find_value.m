function [ locx,locy,table ] = find_value( point,bar,r,g,b,table )


locx=0;
locy=0;

[row,col] = find(bar(:,:,1)== point(:,:,1)&bar(:,:,2)== point(:,:,2)&bar(:,:,3)== point(:,:,3));

if (~(isempty(row))&& ~(isempty(col)))
        
    locx = row(1);
    locy = col(1);
    
    return
    
end

% for i =1:size(bar,1)
%     
%     for j =1:size(bar,2)
%         
%         if (bar(i,j,1)== point(:,:,1) && bar(i,j,2)== point(:,:,2) && bar(i,j,3)== point(:,:,3))
%             
%             locx = i;
%             locy = j;
%             
%         return
%         
%         end
%         
%     end
%     
% end

if (locx==0 && locy==0 && ~(isempty(table)))
    
    [row] = find(table(:,1)== point(:,:,1)&table(:,2)== point(:,:,2)&table(:,3)== point(:,:,3));
    
    if ~(isempty(row))
        
    locx = table(row,4);
    locy = table(row,5);
    
    return
    
    end
    
% for i = 1:size(table,1)
%     
%     if (table(i,1)== point(:,:,1) && table(i,2)== point(:,:,2) && table(i,3)== point(:,:,3))
%             
%             locx = table(i,4);
%             locy = table(i,5);
%             
%         return
%         
%      end
%     
% end

end

if (locx==0 && locy==0)
    
[locx,locy,table] = find_closest(point,r,g,b,table);

end

end