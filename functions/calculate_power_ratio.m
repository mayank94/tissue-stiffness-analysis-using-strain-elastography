function [ ft,PR ] = calculate_power_ratio( result )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

fftresult = fft2(result);
shifted_fft = fftshift(fftresult);
ft = abs(shifted_fft);

TP = (sum(sum(ft)).^2) - (ft(round(size(ft,1)/2),round(size(ft,2)/4))).^2;
LP = 0;

for i = round(size(ft,1)/4) : round(3*size(ft,1)/4)
    
    for j = round(size(ft,2)/4) : round(3*size(ft,2)/4)
        
        LP = LP + ft(i,j);
        
    end
    
end

LP = LP.^2 - (ft(round(size(ft,1)/2),round(size(ft,2)/4))).^2;
HP = TP - LP;
PR = (HP/TP)*100;

end

