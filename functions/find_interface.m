function[interface] = find_interface(colored,stiffness_values,color_bar)

interface_color = colored(1:140,:,:);
[r,g,b] = separate_color_bar(color_bar);
lookup_table = [];

for i = 1:size(interface_color,1)

     for j = 1:size(interface_color,2)
         
         if ((interface_color(i,j,1) + interface_color(i,j,2) + interface_color(i,j,3))<=50)
             
             locx = 0;
             locy = 0;
             
         else

         [locx,locy,lookup_table] = find_value(interface_color(i,j,:),color_bar,r,g,b,lookup_table);
         
         end

            if (locx==0 && locy==0)
            interface(i,j) = 0;
            else
            interface(i,j) = stiffness_values(locx,locy);
            end

     end

 end    
    
