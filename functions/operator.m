function [colored_poly,interface] = operator(colored, handles)

global result;

colored_poly = colored;
        
for i = 1:size(colored_poly,1)/2
    
    if result(i,:) == zeros(1,size(colored_poly,2))
        splitter = i;
        break;
    end
    
end

try
    interface = result(1:splitter,:);
    set(handles.text5, 'Visible', 'off');
catch
    set(handles.text5, 'Visible', 'on');
    splitter = curved_surface(colored);
    interface = result(1:splitter,:);
end

depth = splitter*0.008;
set(handles.text7, 'String', strcat('Interface Depth ',num2str(depth),'cm'));
colored_poly = colored_poly(splitter:size(colored_poly,1),:,:);
result = result(splitter:size(colored,1),:);

for j = 1:size(colored_poly,2)
        
     x = find(result(:,j)>72);
     m = x(find(x>round(2*size(colored_poly,1)/3),1));
     colored_poly(m:size(colored_poly,1),j,:) = 0;
     result(m:size(colored_poly,1),j) = 0;
     
end
    

% for i = 1:size(colored_poly,1)/2
%     
%     for j = 1:size(colored_poly,2)
%     
%     if colored_poly(i,j,:) == 0
%         
%         colored_poly(i,j,:) = 255;
%         
%     end
%     
%     end
%     
% end
% 
% for i = round(2*size(colored_poly,1)/3):size(colored_poly,1)
%     
%     for j = 1:size(colored_poly,2)
%     
%     if result(i,j)>72
%         
%         colored_poly(i,j,:) = 255;
%         
%     end
%     
%     end
% 
%     [x,y] = find(result(i,:)>72);
%     colored_poly(x+i,y,:) = 255;
%     
% end
% 
% for j = 1:size(colored_poly,2)
%     
%     slit = colored_poly(1:size(colored_poly,1)/2,j);
%     [b,a] = find(slit == 255);
%     lowest = max(b);
%     
%     for i = 1:lowest-1
%         colored_poly(i,j,:) = 0;
%     end
%     
% end
% 
% for j = 1:size(colored_poly,2)
%     
%     slit = colored_poly(round(size(colored_poly,1)*2/3):size(colored_poly,1),j);
%     [b,a] = find(slit == 255);
%     highest = min(b);
%     
%     for i = round(size(colored_poly,1)*2/3)+highest+1:size(colored_poly,1)
%         colored_poly(i,j,:) = 0;
%         result(i,j) = 0;
%     end
%     
% end

end