function[ ] = calculate_final_values(inter,x,y,entry,fno,vidsize)

global result;
global result_ab;

res_col = mean(result,1);
ress1 = [mean(res_col),std(res_col)];
res_row = mean(result,2);
ress2 = [mean(res_row),std(res_row)];

resab_col = mean(result_ab,1);
resabs1 = [mean(resab_col),std(resab_col)];
resab_row = mean(result_ab,2);
resabs2 = [mean(resab_row),std(resab_row)];

x(isnan(x)) = 0;
y(isnan(y)) = 0;

x_grad = mean(x,2);
y_grad = mean(y,1);

xs = [mean(x_grad),std(x_grad)];
ys = [mean(y_grad),std(y_grad)];

xys = [sqrt(xs(1).^2+ys(1).^2),atan(ys(1)/xs(1))];

abs_x_grad = sum(abs(x),2);
abs_y_grad = sum(abs(y),1);

abs_xs = [mean(abs_x_grad),std(abs_x_grad)];
abs_ys = [mean(abs_y_grad),std(abs_y_grad)];

abs_xys = [sqrt(abs_xs(1).^2+abs_ys(1).^2),atan(abs_ys(1)/abs_xs(1))];

inter_col = mean(inter,1);
inters1 = [mean(inter_col),std(inter_col)];
inter_row = mean(inter,2);
inters2 = [mean(inter_row),std(inter_row)];

tab = {res_col',ress1',res_row,ress2',resab_col',resabs1',resab_row,resabs2',inter_col',inters1',inter_row,inters2',x_grad,xs',y_grad',ys',xys',abs_x_grad,abs_xs',abs_y_grad',abs_ys',abs_xys'};
for i = 1:size(tab,2)
tab{1,i} = padarray(tab{1,i},500-size(tab{1,i},1),NaN,'post');
end
tab = num2cell(cell2mat(tab));
title1 = {'Mean of relative values by column','Mean and SD of left','Mean of relative values by row','Mean and SD of left'};
title2 = {'Mean of absolute values by column','Mean and SD of left','Mean of absolute values by row','Mean and SD of left'};
title3 = {'Mean of interface values by column','Mean and SD of left','Mean of interface values by row','Mean and SD of left'};
title4 = {'Mean of X-gradients by row','Mean and SD of left','Mean of Y-gradients by column','Mean and SD of left','Net Vector'};
title5 = {'Mean of absolute X-gradients by row','Mean and SD of left','Mean of absolute Y-gradients by column','Mean and SD of left','Absolute net Vector'};
title = horzcat(title1,title2,title3,title4,title5);
tab = vertcat(title,tab);

if entry == 1
xlswrite(strcat('Frame',num2str(fno),'of',num2str(vidsize),'.xlsx'),tab);
else
xlswrite('Stiffness.xlsx',tab,fno);
end

end
