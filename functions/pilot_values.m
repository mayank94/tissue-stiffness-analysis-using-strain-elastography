function[fx,fy,colored] = pilot_values(f,click, handles)
 
lookup_table = [];
 global result;
 global result_ab;
 global interface;

 color_bar = f(271:397,777:792,:);
 [r,g,b] = separate_color_bar(color_bar);

 m = linspace(1,100,numel(color_bar)/3);
 stiffness_values = reshape(m,size(color_bar,2),size(color_bar,1));
 stiffness_values = stiffness_values';
 
 bw_space = f(137:548,186:479,:);
 color_space = f(137:548,483:776,:);
 colored = color_space - bw_space;

 for i = 1:size(colored,1)

     for j = 1:size(colored,2)
         
         if ((colored(i,j,1) + colored(i,j,2) + colored(i,j,3))<=50)
             
             locx = 0;
             locy = 0;
             
         else

         [locx,locy,lookup_table] = find_value(colored(i,j,:),color_bar,r,g,b,lookup_table);
         
         end

            if (locx==0 && locy==0)
            result(i,j) = 0;
            else
            result(i,j) = stiffness_values(locx,locy);
            end

     end

 end
 
 %interface = find_interface(colored,stiffness_values,color_bar);
 
 if click ==1
     
    [colored,interface] = operator(colored, handles);
    
 elseif click ==0
     
    [colored,interface] = manual_op(colored, handles);
    
 end
 
%   for i = 1:size(colored,1)
% 
%      for j = 1:size(colored,2)
%          
%          if ((colored(i,j,1) + colored(i,j,2) + colored(i,j,3))<=50)
%              
%              locx = 0;
%              locy = 0;
%              
%          else
% 
%          [locx,locy,lookup_table] = find_value(colored(i,j,:),color_bar,r,g,b,lookup_table);
%          
%          end
% 
%             if (locx==0 && locy==0)
%             result(i,j) = 0;
%             else
%             result(i,j) = stiffness_values(locx,locy);
%             end
% 
%      end
% 
%   end
  
  mean_interface = mean(interface);
  result_ab = result;
  
  for i = 1:size(result,2)
      
      result(:,i) = result(:,i)./mean_interface(i);
      
  end
 
  
 [fx,fy] = find_gradient(result);
 
%  fx = flipud(fx);
%  fy = flipud(fy);