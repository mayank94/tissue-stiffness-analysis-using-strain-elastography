function [ mag_split ] = find_threshold( result )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

result_resize = reshape(result,size(result,1)*size(result,2),1);

mag_split(1) = numel(find(result_resize == 0));
mag_split(2) = numel(find(result_resize >= 1 & result_resize <= 25));
mag_split(3) = numel(find(result_resize >= 26 & result_resize <= 50));
mag_split(4) = numel(find(result_resize >= 51 & result_resize <= 75));
mag_split(5) = numel(find(result_resize >= 76 & result_resize <= 100));    

end

