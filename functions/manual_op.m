function [colored_poly,interface] = manual_op(colored, handles)

global result;

colored_poly = colored;
        
for i = 1:size(colored_poly,1)/2
    
    if result(i,:) == zeros(1,size(colored_poly,2))
        splitter = i;
        break;
    end
    
end

try
    interface = result(1:splitter,:);
    set(handles.text5, 'Visible', 'off');
catch
    set(handles.text5, 'Visible', 'on');
    splitter = curved_surface(colored);
    interface = result(1:splitter,:);
end

depth = splitter*0.008;
set(handles.text7, 'String', strcat('Interface Depth ',num2str(depth),'cm'));
result = result(splitter:size(colored,1),:);
colored = colored_poly(splitter:size(colored_poly,1),:,:);

imshow(colored);

BW = roipoly(colored);

BW = uint8(BW);

colored_poly = colored;

colored_poly(:,:,1) = colored(:,:,1).*BW;
colored_poly(:,:,2) = colored(:,:,2).*BW;
colored_poly(:,:,3) = colored(:,:,3).*BW;

result = result.*(im2double(BW)*255); 

end