function [ locx,locy,lookup_table ] = find_closest( val,r,g,b,lookup_table )

locx=0;
locy=0;

[~,main_color] = max(val);

ref(1,1:3) = double(val(:,:,1:3));

if main_color == 1
    
    dist = pdist2(double(r(:,1:3)),ref);
    
%     for i = 1:size(r,1)
%         
%         dist(i) = pdist2(ref,double(r(i,1:3)));
% %        dist(i) = ((r(i,1)-val(:,:,1))^2 + (r(i,2)-val(:,:,2))^2 + (r(i,3)-val(:,:,3))^2);
%         
%     end
    
    [~,loc] = min(dist);
    locx = r(loc,4);
    locy = r(loc,5);
    value_store = horzcat(val(:,:,1),val(:,:,2),val(:,:,3),r(loc,4),r(loc,5));
    lookup_table = vertcat(lookup_table,value_store);
    

elseif main_color == 2
    
    dist = pdist2(double(g(:,1:3)),ref);
    
%     for i = 1:size(g,1)
%         
%         dist(i) = pdist2(ref,double(g(i,1:3)));
% %         dist(i) = ((g(i,1)-val(:,:,1))^2 + (g(i,2)-val(:,:,2))^2 + (g(i,3)-val(:,:,3))^2);
%         
%     end
    
    [~,loc] = min(dist);
    locx = g(loc,4);
    locy = g(loc,5);
    value_store = horzcat(val(:,:,1),val(:,:,2),val(:,:,3),g(loc,4),g(loc,5));
    lookup_table = vertcat(lookup_table,value_store);
    
    
elseif main_color == 3
    
    dist = pdist2(double(b(:,1:3)),ref);
    
%     for i = 1:size(b,1)
%        
%         dist(i) = pdist2(ref,double(b(i,1:3)));
% %         dist(i) =((b(i,1)-val(:,:,1))^2 + (b(i,2)-val(:,:,2))^2 + (b(i,3)-val(:,:,3))^2);
%         
%     end
    
    [~,loc] = min(dist);
    locx = b(loc,4);
    locy = b(loc,5);
    value_store = horzcat(val(:,:,1),val(:,:,2),val(:,:,3),b(loc,4),b(loc,5));
    lookup_table = vertcat(lookup_table,value_store);
    
end

end