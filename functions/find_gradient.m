function [ fx,fy ] = find_gradient( result )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

fx = [];
fy = [];

for i = 1:size(result,1)
    
    rowdif = diff(result(i,:));
    fx = vertcat(fx,rowdif);
    
end

for i = 1:size(result,2)
    
    coldif = diff(result(:,i));
    fy = horzcat(fy,coldif);
    
end

fx = horzcat(zeros(size(result,1),1),fx);
fy = vertcat(zeros(1,size(result,2)),fy);

% for i = 1:size(fx,1)
%     
%     for j = 1:size(fx,2)
%         
%         if (fx(i,j)==0 && fy(i,j)==0)
%             
%             fx(i,j)=NaN;
%             fy(i,j)=NaN;
%             
%         end
%         
%     end
%     
% end

%Convert to mm since 125 pixels = 1mm
fx = fx/0.000008;
fy = fy/0.000008;

end

