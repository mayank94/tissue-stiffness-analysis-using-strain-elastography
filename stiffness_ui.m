function varargout = stiffness_ui(varargin)
% STIFFNESS_UI MATLAB code for stiffness_ui.fig
%      STIFFNESS_UI, by itself, creates a new STIFFNESS_UI or raises the existing
%      singleton*.
%
%      H = STIFFNESS_UI returns the handle to a new STIFFNESS_UI or the handle to
%      the existing singleton*.
%
%      STIFFNESS_UI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STIFFNESS_UI.M with the given input arguments.
%
%      STIFFNESS_UI('Property','Value',...) creates a new STIFFNESS_UI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before stiffness_ui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to stiffness_ui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help stiffness_ui

% Last Modified by GUIDE v2.5 14-Dec-2017 17:27:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @stiffness_ui_OpeningFcn, ...
                   'gui_OutputFcn',  @stiffness_ui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before stiffness_ui is made visible.
function stiffness_ui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to stiffness_ui (see VARARGIN)

% Choose default command line output for stiffness_ui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



% This sets up the initial plot - only do when we are invisible
% so window can get raised using stiffness_ui.
% if strcmp(get(hObject,'Visible'),'off')
%     plot(rand(5));
% end

% UIWAIT makes stiffness_ui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = stiffness_ui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
cla;

popup_sel_index = get(handles.popupmenu1, 'Value');
switch popup_sel_index
    case 1
        plot(rand(5));
    case 2
        plot(sin(1:0.01:25.99));
    case 3
        bar(1:.5:10);
    case 4
        plot(membrane);
    case 5
        surf(peaks);
end


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% Obtains the video to load
file = uigetfile('*.avi');
if ~isequal(file, 0)
    obj = VideoReader(file);
    V = read(obj);
    handles.video = V;
    handles.frame_no = 1;
    global f;
    f = V(:,:,:,1);
    imshow(f);
    addpath(genpath(pwd));
    guidata(hObject,handles);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)

function edit1_CreateFcn(hObject, eventdata, handles)
function edit2_CreateFcn(hObject, eventdata, handles)

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% Shows the stiffness values on clicking
     global colored
     axes(handles.axes1);
     imshow(colored);
     datacursormode on
     dcm_obj = datacursormode(gcf);
     set(dcm_obj,'UpdateFcn',@myupdatefcn);

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% Shows X-gradients on the image
     global colored
     global fx
     axes(handles.axes1);
     imshow(colored);
     hold on
     quiver(fx,zeros(size(colored,1),size(colored,2)),20);
     hold off
%      [mag_split] = find_threshold(fx);
%      cnames = {'0','1-25','26-50','51-75','76-100'};
%      rnames = {'Num of pixels'};
%      t = uitable('Parent',gcf,'Data',mag_split,'ColumnName',cnames,'RowName',rnames,'Position',[350 1 550 50]);

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% Shows Y-gradients on the image
    global colored
     global fy
     axes(handles.axes1);
     imshow(colored);
     hold on
     quiver(zeros(size(colored,1),size(colored,2)),fy,20);
     hold off
%      [mag_split] = find_threshold(fy);
%      cnames = {'0','1-25','26-50','51-75','76-100'};
%      rnames = {'Num of pixels'};
%      t = uitable('Parent',gcf,'Data',mag_split,'ColumnName',cnames,'RowName',rnames,'Position',[350 1 550 50]);

% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% Shows XY-gradients on the image
 global colored
     global fy
     global fx
     axes(handles.axes1);
     imshow(colored);
     hold on
     quiver(fx,fy,20);
     hold off
%      mags = sqrt(fx.*fx + fy.*fy);
%      [mag_split] = find_threshold(mags);
%      cnames = {'0','1-25','26-50','51-75','76-100'};
%      rnames = {'Num of pixels'};
%      t = uitable('Parent',gcf,'Data',mag_split,'ColumnName',cnames,'RowName',rnames,'Position',[350 1 550 50]);

% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% Saves everything
global fx
global fy
global interface
calculate_final_values(interface,fx,fy,1,handles.frame_no,size(handles.video,4));

% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% Crops automatically
click = 1;
global f;
global colored;
global fx;
global fy;
global result_ab;
global result;
[fx,fy,colored] = pilot_values(f,click, handles);
imshow(colored);
[mag_split] = find_threshold(result_ab);
[~,PR] = calculate_power_ratio(result);
value = horzcat(mag_split,PR);
cnames = {'0','1-25','26-50','51-75','76-100','Power Ratio'};
rnames = {'Value'};
t = uitable('Parent',gcf,'Data',value,'ColumnName',cnames,'RowName',rnames,'Position',[150 1 550 60]);


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% Crops manually
click = 0;
global f;
global colored;
global result_ab;
global result;
global fx;
global fy;
[fx,fy,colored] = pilot_values(f,click, handles);
imshow(colored);
[mag_split] = find_threshold(result_ab);
[~,PR] = calculate_power_ratio(result);
value = horzcat(mag_split,PR);
cnames = {'0','1-25','26-50','51-75','76-100','Power Ratio'};
rnames = {'Value'};
t = uitable('Parent',gcf,'Data',value,'ColumnName',cnames,'RowName',rnames,'Position',[150 1 550 60]);


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
%Display the power ratio
global result;
[ft] = calculate_power_ratio(result);
imshow(ft.*50,[24 100000]);


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% Displays the intensity distribution
global result
Im = mat2gray(result);
imhist(Im);

function edit1_Callback(hObject, eventdata, handles)
handles.begin = get(handles.edit1,'String');
guidata(hObject,handles);

function edit2_Callback(hObject, eventdata, handles)
handles.finish = get(handles.edit2,'String');
guidata(hObject,handles);

% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% Finds values in a specific range
global result;
global colored;

start_range = str2num(handles.begin);
stop_range = str2num(handles.finish);
[x,y] = find(result>=start_range & result<=stop_range);
found_range = colored;

for i=1:numel(x)
    
    found_range(x(i),y(i),:) = 255;
    
end

imshow(found_range);


% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% Goes one frame back
global f;
fno = handles.frame_no;
if (fno-str2num(handles.delta))>1
    fno = fno - str2num(handles.delta);
end
handles.frame_no = fno;
V = handles.video;
f = V(:,:,:,fno);
imshow(f);
h = uicontrol('Style','text','String',strcat(num2str(fno),'/',num2str(size(V,4))));
guidata(hObject,handles);

% --- Executes on button press in pushbutton15.
function pushbutton16_Callback(hObject, eventdata, handles)
% Goes one frame forward
global f;
fno = handles.frame_no;
V = handles.video;
if (fno+str2num(handles.delta)-1)<size(V,4)
    fno = fno + str2num(handles.delta);
end
handles.frame_no = fno;
f = V(:,:,:,fno);
imshow(f);
h = uicontrol('Style','text','String',strcat(num2str(fno),'/',num2str(size(V,4))));
guidata(hObject,handles);


% --- Executes on button press in pushbutton17.
function pushbutton17_Callback(hObject, eventdata, handles)
%Computes a file for the values of the entire video
V = handles.video;
for i = 1:size(V,4)
    [fx,fy,~,interface] = pilot_values(V(:,:,:,i),1);
    calculate_final_values(interface,fx,fy,2,i,size(V,4));
end
    


% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
%Show map of absolute values
global result_ab
imshow(result_ab,[]);


% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
%Show map of relative values
global result
imshow(result(:,1:size(result,2)-1),[]);


% --- Executes on button press in pushbutton20.
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global fx;
imshow(abs(fx(:,1:size(fx,2)-1)),[]);

% --- Executes on button press in pushbutton21.
function pushbutton21_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global fy;
imshow(abs(fy(:,1:size(fy,2)-1)),[]);

% --- Executes on button press in pushbutton22.
function pushbutton22_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global fx;
global fy;
fx1 = fx(:,1:size(fx,2)-1);
fy1 = fy(:,1:size(fy,2)-1);
imshow(sqrt(fx1.*fx1+fy1.*fy1),[]);



function edit5_Callback(hObject, eventdata, handles)
handles.delta = get(handles.edit5,'String');
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
str = get(hObject,'str');
handles.delta = str;
guidata(hObject,handles);
